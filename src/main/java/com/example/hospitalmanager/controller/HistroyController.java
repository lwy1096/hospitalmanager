package com.example.hospitalmanager.controller;


import com.example.hospitalmanager.entity.HospitalPatient;
import com.example.hospitalmanager.model.HistoryItem;
import com.example.hospitalmanager.model.HistoryRequest;
import com.example.hospitalmanager.service.HospitalPatientService;
import com.example.hospitalmanager.service.PrescribeHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")

public class HistroyController {
    private final HospitalPatientService hospitalPatientService;
    private final PrescribeHistoryService prescribeHistoryService;
    @PostMapping("/new/customer/id/{patientid}")
    public String setHistory(@PathVariable long patientid, @RequestBody @Valid HistoryRequest request) {
        HospitalPatient hospitalPatient = hospitalPatientService.getData(patientid);
        prescribeHistoryService.setPrescribeHistory(hospitalPatient, request);
        return "thanks";
    }

    @GetMapping("/all/date")
    public List<HistoryItem> getAllData(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        List<HistoryItem> result = prescribeHistoryService.getHistoryList(searchDate);
        return result;
    }
}
