package com.example.hospitalmanager.controller;

import com.example.hospitalmanager.entity.HospitalPatient;
import com.example.hospitalmanager.model.HospitalPatientRequest;
import com.example.hospitalmanager.service.HospitalPatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/patient")
@RequiredArgsConstructor
public class PatientController {
    private final HospitalPatientService hospitalPatientService;

    @PostMapping("/enroll")
    public String setPatient(@RequestBody @Validated  HospitalPatientRequest request) {
        hospitalPatientService.setHospitalPatient(request);

        return "Thanks";
    }
}
