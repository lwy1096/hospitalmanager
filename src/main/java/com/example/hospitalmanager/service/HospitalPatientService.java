package com.example.hospitalmanager.service;

import com.example.hospitalmanager.entity.HospitalPatient;
import com.example.hospitalmanager.model.HospitalPatientRequest;
import com.example.hospitalmanager.repository.HospitalPatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HospitalPatientService {
    private final HospitalPatientRepository hospitalPatientRepository;

    public void setHospitalPatient(HospitalPatientRequest request) {
        HospitalPatient addData = new HospitalPatient.HospitalPatientBuilder(request).build();
        hospitalPatientRepository.save(addData);
    }

    public HospitalPatient getData(long id) {
        return hospitalPatientRepository.findById(id).orElseThrow();
    }
}
