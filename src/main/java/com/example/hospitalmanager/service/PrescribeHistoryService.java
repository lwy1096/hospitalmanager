package com.example.hospitalmanager.service;

import com.example.hospitalmanager.entity.HospitalPatient;
import com.example.hospitalmanager.entity.PrescribeHistory;
import com.example.hospitalmanager.model.HistoryItem;
import com.example.hospitalmanager.model.HistoryRequest;
import com.example.hospitalmanager.repository.PrescribeHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PrescribeHistoryService {
    private final PrescribeHistoryRepository prescribeHistoryRepository;
    public void setPrescribeHistory(HospitalPatient hospitalPatient, HistoryRequest request) {
        PrescribeHistory addData = new PrescribeHistory();
        addData.setHospitalPatient(hospitalPatient);
        addData.setTreatmentList(request.getTreatmentList());
        addData.setPrice((request.getIsInsuranced() ? request.getTreatmentList().getInsurancedPrice() : request.getTreatmentList().getNonInsurancedPrice()));
        addData.setIsInsuranced(request.getIsInsuranced());
        addData.setTreatmentDate(LocalDate.now());
        addData.setTreatmentTime(LocalTime.now());
        addData.setIsPaid(false);

        prescribeHistoryRepository.save(addData);
    }
    public List<HistoryItem> getHistoryList(LocalDate searchDate) {
        List<PrescribeHistory> originList = prescribeHistoryRepository.findAllByTreatmentDateOrderByIdDesc(searchDate);

        List<HistoryItem> result = new LinkedList<>();
        for(PrescribeHistory item :originList) {
            HistoryItem addData = new HistoryItem();
            addData.setHistoryId(item.getId());
            addData.setPatientId(item.getHospitalPatient().getId());
            addData.setPatientName(item.getHospitalPatient().getPatientName());
            addData.setIdNumber(item.getHospitalPatient().getIdNumber());
            addData.setPhoneNum(item.getHospitalPatient().getPhoneNum());
            addData.setTreatmentName(item.getTreatmentList().getTreatmentName());
            addData.setTreatmentDate(item.getTreatmentDate());
            addData.setTreatmentTime(item.getTreatmentTime());
            addData.setIsInsuranced((item.getIsInsuranced()? "예":"아니오"));
            addData.setPrice(item.getPrice());
            addData.setEnrollDate(item.getHospitalPatient().getEnrollDate());
            addData.setIsPaid((item.getIsPaid()?"예":"아니오"));

            result.add(addData);

        }
        return result;
    }

}
