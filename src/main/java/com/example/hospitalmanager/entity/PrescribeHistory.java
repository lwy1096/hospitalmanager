package com.example.hospitalmanager.entity;

import com.example.hospitalmanager.enums.TreatmentList;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.repository.cdi.Eager;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class PrescribeHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospitalPatientId", nullable = false)
    private HospitalPatient hospitalPatient;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private TreatmentList treatmentList;

    @Column(nullable = false)
    private Integer price;

    @Column(nullable = false)
    private Boolean isInsuranced;

    @Column(nullable = false)
    private LocalTime treatmentTime;
    @Column(nullable = false)
    private LocalDate treatmentDate;
    @Column(nullable = false)
    private Boolean isPaid ;

}
