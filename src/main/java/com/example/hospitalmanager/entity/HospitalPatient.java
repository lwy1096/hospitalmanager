package com.example.hospitalmanager.entity;

import com.example.hospitalmanager.interfaces.CommonModelBuilder;
import com.example.hospitalmanager.model.HospitalPatientRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HospitalPatient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,length = 15)
    private String patientName;

    @Column(nullable = false, length = 14)
    private String idNumber;

    @Column(nullable = false,length = 13)
    private String phoneNum;
    @Column(nullable = false)
    private String address;

    @Column(nullable = false , columnDefinition = "TEXT")
    private String purpose;
    @Column(nullable = false)
    private LocalDate enrollDate;

private HospitalPatient(HospitalPatientBuilder builder) {
    this.patientName = builder.patientName;
    this.idNumber = builder.idNumber;
    this.phoneNum = builder.phoneNum;
    this.address = builder.address;
    this.purpose = builder.purpose;
    this.enrollDate = builder.enrollDate;
}
public static class HospitalPatientBuilder implements CommonModelBuilder<HospitalPatient> {
    //static 고정된
    private  final String patientName;
    private  final String idNumber;
    private  final String phoneNum;
    private  final String address;
    private final  String purpose;
    private final  LocalDate enrollDate;


    public HospitalPatientBuilder(HospitalPatientRequest request) {
        this.patientName = request.getPatientName();
        this.idNumber = request.getIdNumber();
        this.phoneNum = request.getPhoneNum();
        this.address = request.getAddress();
        this.purpose = request.getPurpose();
        this.enrollDate = LocalDate.now();
    }

    @Override
    public HospitalPatient build() {
        return new HospitalPatient(this);
    }
}


}
