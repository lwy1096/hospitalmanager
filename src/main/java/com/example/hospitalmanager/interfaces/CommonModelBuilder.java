package com.example.hospitalmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}

