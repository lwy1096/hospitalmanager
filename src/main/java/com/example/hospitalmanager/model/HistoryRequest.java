package com.example.hospitalmanager.model;

import com.example.hospitalmanager.enums.TreatmentList;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HistoryRequest {

    @Enumerated(value = EnumType.STRING)
    private TreatmentList treatmentList;

    @NotNull
    private Boolean isInsuranced;
}
