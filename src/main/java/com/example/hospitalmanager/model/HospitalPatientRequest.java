package com.example.hospitalmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class HospitalPatientRequest {
    @NotNull
    @Length(min =2 , max = 15)
    private String patientName;

    @NotNull
    @Length(min =9 , max = 14)
    private String idNumber;

    @NotNull
    @Length(min =2 , max = 100)
    private String address;
    @NotNull
    @Length(min =5 , max = 13)
    private String phoneNum;

    @NotNull
    @Length(min =2)
    private String purpose;


}
