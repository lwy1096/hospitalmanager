package com.example.hospitalmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter

public class HistoryItem {
    private Long historyId;
    private Long patientId;
    private String patientName;
    private String idNumber;

    private String phoneNum;

    private String treatmentName;

    private LocalDate treatmentDate;
    private LocalTime treatmentTime;

    private String isInsuranced;
    private Integer price;
    private LocalDate enrollDate;
    private String isPaid ;

}
