package com.example.hospitalmanager.repository;

import com.example.hospitalmanager.entity.HospitalPatient;
import org.hibernate.metamodel.model.convert.spi.JpaAttributeConverter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalPatientRepository extends JpaRepository<HospitalPatient, Long> {
}
