package com.example.hospitalmanager.repository;

import com.example.hospitalmanager.entity.PrescribeHistory;
import org.hibernate.metamodel.model.convert.spi.JpaAttributeConverter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface PrescribeHistoryRepository extends JpaRepository<PrescribeHistory,Long> {
    List<PrescribeHistory> findAllByTreatmentDateOrderByIdDesc(LocalDate searchDate);
}
