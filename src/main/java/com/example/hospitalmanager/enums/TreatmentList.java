package com.example.hospitalmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TreatmentList {
    CHUNA("추나요법", 70000, 70000),
    PHSIO("물리치료", 15000,5000),

    ACUPUNCTURE("침치료", 20000, 7000)
    ;
    private final String treatmentName;
    private final Integer nonInsurancedPrice;
    private final Integer insurancedPrice;
}
